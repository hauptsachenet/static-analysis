# Static analysis tools for (our) php projects

Currently included are:

- phpmd/phpmd `docker run --rm -v$PWD:$PWD -w$PWD static-analysis phpmd src/ text codesize`
- sensiolabs/security-checker `docker run --rm -v$PWD:$PWD -w$PWD static-analysis security-checker security:check`
